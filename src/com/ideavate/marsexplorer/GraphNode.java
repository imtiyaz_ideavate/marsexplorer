package com.ideavate.marsexplorer;

/**
 *	define a node on graph
 */
public class GraphNode {

	private int x;
	private int y;
	
	public GraphNode(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public String getName() {
		return "["+x+" , "+y+"]";
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return (x+y)/10;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		GraphNode graphNode = (GraphNode) obj;
		return graphNode.x == this.x && graphNode.y == this.y;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "(" + x + "," + y + ")";
	}
}

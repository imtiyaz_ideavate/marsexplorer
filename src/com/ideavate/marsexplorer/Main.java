package com.ideavate.marsexplorer;

import java.util.Scanner;
import com.ideavate.marsexplorer.algo.ShortestPath;
import com.ideavate.marsexplorer.utils.AppConstant;
import com.ideavate.marsexplorer.utils.CommonUtils;

/**
 * class which is the entry point of project. 
 */
public class Main {

	public static void main(String[] args) {
		
		CommonUtils.showIntroduction();
		
		ShortestPath shortestPath = new ShortestPath();
		ExplorerTableTop explorerTableTop = new ExplorerTableTop(shortestPath);
		MarsExplorer explorer = new MarsExplorer(explorerTableTop);
		CommandHandler handler = new CommandHandler(explorer);
		
		try(Scanner scanner = new Scanner(System.in)){
			String command = "exit";
			do {
				command = scanner.nextLine();
				handler.processCommands(command);
				System.out.println("\nEnter next input ?");
			} while(!command.equalsIgnoreCase(AppConstant.EXIT));
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Program terminated...!!!");
	}
}

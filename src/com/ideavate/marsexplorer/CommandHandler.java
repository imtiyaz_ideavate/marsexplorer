package com.ideavate.marsexplorer;

import com.ideavate.marsexplorer.utils.AppConstant;

/**
 * class to handle commands only. 
 */

public class CommandHandler {
	
	private MarsExplorer marsExplorer;
	private boolean isFirstCommand = true;
	
	public CommandHandler(MarsExplorer marsExplorer){
		this.marsExplorer = marsExplorer;
	}
	
	/**
	 * This is the processCommands method is used to process all commands and process accordingly. 
	 * @param command 
	 * @return Nothing.
	 */
	public void processCommands(String command){
				
		String commandName = getCommandName(command);
		
		if(isFirstCommand){
			if(commandName.equalsIgnoreCase(AppConstant.PLACE)){
				marsExplorer.placeExplorerTo(getValue(command));
				isFirstCommand = false;
			} else {
				if(!command.equalsIgnoreCase(AppConstant.EXIT))
					System.out.println("The first valid command to the explorer is a PLACE command..!!");
			}
		} else {
			if(commandName.equalsIgnoreCase(AppConstant.BLOCK)){
				marsExplorer.blockPosition(getValue(command));
			}else if(commandName.equalsIgnoreCase(AppConstant.EXPLORE)){
				marsExplorer.moveExplorerTo(getValue(command));
			}else if(commandName.equalsIgnoreCase(AppConstant.PLACE)){
				marsExplorer.placeExplorerTo(getValue(command));
			}else if(command.equalsIgnoreCase(AppConstant.REPORT)){
				marsExplorer.reportExplorerAndBlocks();
			}else if(command.equalsIgnoreCase(AppConstant.EXIT)){
				//so that wrong input message is not displayed..
			} else {
				System.out.println("Wrong input..!!");
			}			
		}
	}

	
	/**
	 * This is the getValue method extract the node value from command. 
	 * @param command 
	 * @return node value like (x,y).
	 */
	private String getValue(String command) {
		
		String value = null;
		if(command != null){
			int beginIndex = command.indexOf(" ");
			value = command.substring(beginIndex, command.length());
		}
		return value;
	}

	/**
	 * This is the getCommandName method extract command name from command for 
	 * eg. BLOCK 1,1 then retunr BLOCK. 
	 * @param command 
	 * @return command name like BLOCK/EXPLORE etc.
	 */
	private String getCommandName(String command) {
		
		String commandName = "";
		if(command != null && command.length()>0){
			int indx = command.indexOf(" ");
			if(indx != -1){
				commandName = command.substring(0, indx);	
			}
		}
		return commandName;
	}
}

package com.ideavate.marsexplorer.algo;

/* Import statements */
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.io.FileReader;
import java.io.BufferedReader;


public class ShortestPath {
	private List<Edge> vEdgeList = null;
	private	int noOfNodes = 0;
	private List<GraphNode> hDistanceStore = null;
	private Map<String, List<String>> pathStr = null;
	
	/**
	* Class to store each
	*/
	class Edge {
		public String edgeName;
		public int edgeWeight;
	};

	/**
	* Class to store vertex
	*/
	class GraphNode {
		public String nodeName;
		public int nodeDistance;
	};

	/**
	* Returns the Adjacency List
	*/
	public List<Edge> getEdgeList () {
		return vEdgeList;
	}

	/**
	* Read the contents of the file into hashtable
	*/
	public void readFileContents (String fileName) {
		/* Initialise the hashtable */
		vEdgeList = new ArrayList<>();
		hDistanceStore = new ArrayList<>();
		Edge eEdgeInfo = null;
		GraphNode nNodeInfo = null;
		String line = "";
		String edgeName = "";
		String vertexName = "";
		String tempType = "";
		int i = 0;

		try {
			/* Opening the file buffer */
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader =new BufferedReader(fileReader);

			/* The first line indicates the no of nodes in the graph */
			line = bufferedReader.readLine();
			noOfNodes = Integer.parseInt (line);

			for(i = 0; i < noOfNodes; i++) {
				line = bufferedReader.readLine();
				StringTokenizer sTokens = new StringTokenizer(line);

				/* The first token is the vertex name. */
				vertexName = sTokens.nextToken();

				/* Adding to the hDistanceStore */
				nNodeInfo = new GraphNode();
				nNodeInfo.nodeName = vertexName;

				/* Initialising the distance to (+)ve INFINITY */
				nNodeInfo.nodeDistance = 99999;

				hDistanceStore.add(nNodeInfo);

				/* Skipping the second token, which indicates the count of the neighbours */
				tempType = sTokens.nextToken();

				/* Now reading the actual neighbours */
				while (sTokens.hasMoreTokens()) {
					eEdgeInfo = new Edge();
					/* Get the neighbour name */
					tempType = sTokens.nextToken();
					edgeName = vertexName + "-" + tempType;

					/* Get the edge weight */
					tempType = sTokens.nextToken();

					/* Populate an eEdgeInfo object */
					eEdgeInfo.edgeName = edgeName;
					eEdgeInfo.edgeWeight = Integer.parseInt (tempType);

					/* Store all the edges one by one */
					vEdgeList.add(eEdgeInfo);
				}
			}

			/* Closing the file buffer */
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* Processes the data structure hDistanceStore */
	private GraphNode locateGraphNode(String nodeName) {
		boolean bFound = false;
		int i = 0;
		for (i = 0; i < hDistanceStore.size(); i++) {

			if ( ((GraphNode)hDistanceStore.get(i)).nodeName.equals (nodeName) ) {
				bFound = true;
				break;
			}
		}
		if (bFound) {
			return (GraphNode)hDistanceStore.get(i);
		}
		return null;
	}

	/**
	Assert function
	*/
	public void display () {
		for (int i = 0; i < vEdgeList.size(); i++) {
			System.out.print ("vEdgeList[ " + i + "] : " + ((Edge)vEdgeList.get(i)).edgeName);
			System.out.println("   Weight : " + ((Edge)vEdgeList.get(i)).edgeWeight);
		}
	}

	/**
	 * This function is the implementation of Bellman Ford algorithm. This function will find shortest
	 * path.
	 *
	 * @startingNode The source node for computing paths
	 */
	public void shortestPath(String startingNode, GraphNode target) {
		Edge eTempEdge = null;
		StringTokenizer stTokens = null;
		String sEdgeName = "";
		String sinkName = "";
		String sourceName = "";
		GraphNode gSource = null;
		GraphNode gSink = null;
		boolean bFound = false;
		boolean breakInnerLoop = false, breakOuterLoop = false;
		pathStr = new HashMap<>();

		/* Set the starting node's distance as ZERO */
		gSource = locateGraphNode(startingNode);
		gSource.nodeDistance = 0;

		int ivCount = 0;
		boolean bChanged = false;
		for (ivCount = 0; ivCount < noOfNodes - 1; ivCount++) {
			for (int ieCount = 0; ieCount < vEdgeList.size(); ieCount++) {
				eTempEdge = (Edge)vEdgeList.get(ieCount);
				sEdgeName = eTempEdge.edgeName;
				stTokens = new StringTokenizer(sEdgeName, "-");
				while (stTokens.hasMoreTokens()) {
					try {
						sourceName = (String)stTokens.nextToken();
						sinkName = (String)stTokens.nextToken();
						gSource = locateGraphNode(sourceName);
						gSink = locateGraphNode(sinkName);
						
						/* Applying the conditions */
						if (gSink != null && gSink.nodeDistance > (gSource.nodeDistance + eTempEdge.edgeWeight) ) {
							gSink.nodeDistance = gSource.nodeDistance + eTempEdge.edgeWeight;
							bChanged = true;
							
							List<String> temp1 = pathStr.get(gSink.nodeName);
							if(temp1 == null){
								temp1 = new ArrayList<>();
							}			
							List<String> oldList = pathStr.get(gSource.nodeName);
							if(oldList != null) temp1.addAll(oldList);
							temp1.add(eTempEdge.edgeName);
							pathStr.put(gSink.nodeName, temp1);
							
							if(target.nodeName.equalsIgnoreCase(gSink.nodeName)){
								breakOuterLoop = true;
								breakInnerLoop = true;
								break;
							}
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				if(breakInnerLoop) break;
			}

			/* Now if none of the values are changed in an iteration,
			then we can break out of the loop to stop further processing.*/
			if (bChanged == false) {
				break;
			}

			/* Resetting the flag for next iteration. */
			bChanged = false;
			
			if(breakOuterLoop) break;
		}

		// Uncomment to see shortest path from source to all nodes
/*		Set<String> tempKeys = pathStr.keySet();
		for(String temp2 : tempKeys){
			System.out.println(temp2 +" : "+pathStr.get(temp2));
		}
*/		
		/* Now checking if there were solutions found or not */
		/* Separate the source and sink for each edge */
		for (ivCount = 0; ivCount < vEdgeList.size(); ivCount++) {
			eTempEdge = (Edge)vEdgeList.get(ivCount);
			sEdgeName = eTempEdge.edgeName;
			stTokens = new StringTokenizer(sEdgeName, "-");
			while (stTokens.hasMoreTokens()) {
				try {
					sourceName = (String)stTokens.nextToken();
					sinkName = (String)stTokens.nextToken();
					gSource = locateGraphNode(sourceName);
					gSink = locateGraphNode(sinkName);

					/* Applying the conditions */
					if (gSink.nodeDistance > gSource.nodeDistance + eTempEdge.edgeWeight) {
						bFound = true;
						break;
					}
				} catch(Exception e) {
//					e.printStackTrace();
				}
				if (bFound) {
					break;
				}
			}
		}

//		System.out.println("\nNode names with their distances from the Node :: "+startingNode);

		/* Printing the distances */
/*		for (ivCount = 0; ivCount < hDistanceStore.size(); ivCount++) {
			System.out.print ("NodeName : " + ((GraphNode)hDistanceStore.get(ivCount)).nodeName	);
			System.out.println("   Distance : " + ((GraphNode)hDistanceStore.get(ivCount)).nodeDistance);
		}
*/	}
	
	public void initShortestPathAlgo(String filePath, String startingNode, com.ideavate.marsexplorer.GraphNode targetNode){

		GraphNode target = new GraphNode();
		target.nodeName = targetNode.toString();
		
		/* Read the input */
		readFileContents(filePath);
		getEdgeList();

		/* Executing DFS on undirected graph */
		shortestPath(startingNode, target);
	}

	public Map<String, List<String>> getPathStr() {
		return pathStr;
	}

	public void setPathStr(Map<String, List<String>> pathStr) {
		this.pathStr = pathStr;
	}
}
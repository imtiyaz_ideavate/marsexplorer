package com.ideavate.marsexplorer.utils;

/**
 * This is Utility class to perfomr some specific task.
 */
public class CommonUtils {

	
	/**
	 * To print * in a line.
	 */
	private static void printCharInline(){
		for(int i=0;i<50;i++){
			System.out.print("*");
		}
		System.out.println("");
	}
		
	/**
	 * To print white space vertically
	 */
	private static void createVerticalSpace(int times){
		for(int i=0; i< times ; i++){
			System.out.println("");
		}
	}
	
	/**
	 * Project Introduction screen.
	 */
	public static void showIntroduction(){

		printCharInline();
		System.out.println("Welcome to Mars Explorer Simulator");
		createVerticalSpace(2);
		System.out.println("Following are the commands available to operate MarsExplorer.");
		System.out.println("MarsExplorer will explore in 5X5 area only.");
		System.out.println("Please use the commands in the same format like below.");
		createVerticalSpace(1);
		System.out.println("1. PLACE X,Y => will put the explorer on the table in position X,Y");
		System.out.println("2. BLOCK X,Y => will put the obstruction object to the unit of given position X, Y");
		System.out.println("3. EXPLORE X,Y => will find the shortest path to move from the original position to the target position X,Y");
		System.out.println("4. REPORT => will announce the X,Y of the explorer, and the positions of blocks.");
		System.out.println("5. EXIT => To quit program.");
		
		createVerticalSpace(2);
		System.out.println("Please enter your option.");		
	}
}

package com.ideavate.marsexplorer.utils;


/**
 * Interface to define constant that will be used througout project
 */
public interface AppConstant {

    String PLACE = "PLACE";
    String BLOCK = "BLOCK";
    String REPORT = "REPORT";
    String EXPLORE = "EXPLORE";
    String EXIT = "Exit"; 
    String ADJACENCY_LIST_FILE_NAME = "adjancy_list.txt";
}
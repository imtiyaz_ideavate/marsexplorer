package com.ideavate.marsexplorer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ideavate.marsexplorer.algo.ShortestPath;
import com.ideavate.marsexplorer.utils.AppConstant;

/**
 * The class stores the information related to table top, like dimension, block nodes etc. 
 */
public class ExplorerTableTop {

	public static final int width = 5;
	public static final int height = 5;
	public static final int nodeWeight = 1;
	private GraphNode explorerCurrentPosition;
	private Set<GraphNode> blocks = new HashSet<>();
	private ShortestPath shortestPath;
	private Map<String, List<String>> shortestPathMap = null;
	
	/**
	 * function returns the shortest path
	 * @param shortestPath
	 */
	public ExplorerTableTop(ShortestPath shortestPath){
		this.shortestPath = shortestPath;
	}
	
	/**
	 * returns all blocks 
	 */
	public Set<GraphNode> getBlocks() {
		return blocks;
	}

	/**
	 * add block to existing block list 
	 */
	public void setBlocks(Set<GraphNode> blocks) {
		this.blocks = blocks;
	}
	
	/**
	 * returns explorer current position  
	 */
	public GraphNode getExplorerCurrentPosition() {
		return explorerCurrentPosition;
	}

	/**
	 * returns all blocks 
	 * @param shortestPath
	 */
	public void setExplorerCurrentPosition(GraphNode explorerCurrentPosition) {
		this.explorerCurrentPosition = explorerCurrentPosition;
	}

	/**
	 * create a fresh adjancency list for Bellman Ford algorithm.
	 * @param targetNode
	 */
	public void refreshAdjancyList(GraphNode targetNode){
		
		boolean isAdjacencyListRefreshed = false;
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(AppConstant.ADJACENCY_LIST_FILE_NAME))){
			
			int rowCounter = 0;
			StringBuffer sb = new StringBuffer();
			
			for(int row= 0 ; row < width ; row++){
				for(int col = 0 ; col< height; col++){
					String leftNode = null, rightNode = null, topNode = null, bottomNode = null, currNode = null;
					int neighbourCount = 0;
					
					if(!blocks.contains(new GraphNode(row, col))){
						currNode = "("+row+","+col+")";
						
						if(row-1>=0){ //adding left adjacent
							if(!blocks.contains(new GraphNode(row-1, col))){
								leftNode = "("+(row-1)+","+col+")";
								neighbourCount++;							
							}
						}
						if(row+1>=0){ //adding right adjacent
							if(!blocks.contains(new GraphNode(row+1, col))){
								rightNode = "("+(row+1)+","+col+")";
								neighbourCount++;
							}
						}
						if(col+1>=0){ //adding top adjacent
							if(!blocks.contains(new GraphNode(row, col+1))){
								topNode = "("+row+","+(col+1)+")";
								neighbourCount++;
							}						
						}
						if(col-1>=0){ //adding bottom adjacent
							if(!blocks.contains(new GraphNode(row, col-1))){
								bottomNode = "("+row+","+(col-1)+")";
								neighbourCount++;							
							}
						}
						
						sb.append(currNode+" "+neighbourCount);
						if(leftNode != null) sb.append(" "+ leftNode + " " + nodeWeight);
						if(rightNode != null) sb.append(" "+ rightNode + " " + nodeWeight);
						if(topNode != null) sb.append(" "+ topNode + " " + nodeWeight);
						if(bottomNode != null) sb.append(" "+ bottomNode + " " + nodeWeight);
						sb.append(System.getProperty("line.separator"));
						rowCounter++;
					}
				}				
			}
			
			bw.write(Integer.toString(rowCounter));
			bw.newLine();			
			bw.write(sb.toString());	
			isAdjacencyListRefreshed = true;
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		if(isAdjacencyListRefreshed){
			shortestPath.initShortestPathAlgo(AppConstant.ADJACENCY_LIST_FILE_NAME, explorerCurrentPosition.toString(), targetNode);
			shortestPathMap = shortestPath.getPathStr();
		}
	}
	
	/**
	 * return the shortest path from source to destination
	 * @param targetNode
	 * @return the shortest path
	 */
	public List<String> getShortestPathMap(GraphNode targetNode) {
		return shortestPathMap.get(targetNode.toString());
	}

	public static void main(String args[]){
		
		ShortestPath shortestPath = new ShortestPath();
		ExplorerTableTop explorerTableTop = new ExplorerTableTop(shortestPath);
		explorerTableTop.refreshAdjancyList(null);
		System.out.println("Adjancy list refreshed...!!");
	}
}

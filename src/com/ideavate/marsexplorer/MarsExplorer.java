package com.ideavate.marsexplorer;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * class to handle all operation of Explorer, like place, block, explore and report.
 */
public class MarsExplorer {

	// to store current location of explorer.
	private GraphNode explorerCurrentPosition;
	// refrence of table top for Explorer.
	private ExplorerTableTop explorerTableTop;
	
	public MarsExplorer(ExplorerTableTop explorerTableTop){
		explorerCurrentPosition = new GraphNode(0,0);
		this.explorerTableTop = explorerTableTop;
		explorerTableTop.setExplorerCurrentPosition(explorerCurrentPosition);
	}

	/**
	 * The function is responsible to print current location of Explorer and all blocks.
	 */
	public void reportExplorerAndBlocks() {
		
		System.out.println("Place : "+explorerCurrentPosition.toString());
		System.out.print("Blocks : ");
		for(GraphNode graphNode : explorerTableTop.getBlocks()){
			System.out.print(graphNode);
		}
		System.out.println("");
	}

	/**
	 * will place the explorer to given node provide its not a blocking node.
	 * @param node : destination node
	 */
	public void placeExplorerTo(String node) {
		
		GraphNode temp = getGraphNodeFromString(node);
		if(temp != null){
			if(!explorerTableTop.getBlocks().contains(temp)){
				explorerCurrentPosition.setX(temp.getX());
				explorerCurrentPosition.setY(temp.getY());							
			}else{
				System.out.println("Explorer can not be placed on a blocking node...!!");
			}
		}
	}

	/**
	 * will move the explorer to given destination node
	 * @param node : destination node
	 */
	public void moveExplorerTo(String node) {
		
		GraphNode tempGraphNode = getGraphNodeFromString(node);
		if(tempGraphNode != null){ // to ensure that dest node is not a falling node.   
			if(!explorerTableTop.getBlocks().contains(tempGraphNode)){ //to check if the dest node is not among blocking nodes
				explorerTableTop.refreshAdjancyList(tempGraphNode);
				List<String> path = explorerTableTop.getShortestPathMap(tempGraphNode);
				System.out.print("Path : ");
				if(path != null){
					updatePathInfo(path);
					explorerCurrentPosition = tempGraphNode;
					explorerTableTop.setExplorerCurrentPosition(explorerCurrentPosition);					
				}
			}
		}
	}

	/**
	 * will print the path from source to destination
	 * @param path : list of nodes between source and destination
	 */
	private void updatePathInfo(List<String> path) {
		// TODO Auto-generated method stub
				
		if(path != null){
			Set<String> set = new LinkedHashSet<>();
			for(String item : path){
				StringTokenizer st = new StringTokenizer(item, "-");
				if(st.hasMoreTokens()) set.add(st.nextToken());
				if(st.hasMoreTokens()) set.add(st.nextToken());
			}
			
			for(String item : set){
				System.out.print(" "+item);
			}
			System.out.println(""); // extra new line for space			
		}
	}

	/**
	 * will add node to the existing blocking node
	 * @param node : blocking node
	 */
	public void blockPosition(String node) {
		
		GraphNode tempGraphNode = getGraphNodeFromString(node);
		if(tempGraphNode != null){
			if(!tempGraphNode.equals(explorerCurrentPosition))
				explorerTableTop.getBlocks().add(tempGraphNode);
			else{
				System.out.println("Explorer node can never be blocked..!!");
			}
		}
	}	
	
	/**
	 * creates GraphNode object from string node value.
	 * @param node : any node
	 * @return : GraphNode object
	 */
	private GraphNode getGraphNodeFromString(String node){

		GraphNode tempGraphNode = null;
		try{
			StringTokenizer tokenizer = new StringTokenizer(node);
			
			int x =-1 ,y =-1;
			
			if(tokenizer.hasMoreTokens()) x = Integer.parseInt(tokenizer.nextToken(",").trim());
			if(tokenizer.hasMoreTokens()) y = Integer.parseInt(tokenizer.nextToken(",").trim());
	
			// check if user is putting the Exploere within TableTop area.
			if( x >= 0 && x < ExplorerTableTop.width &&  y >= 0 && y < ExplorerTableTop.height){
				tempGraphNode = new GraphNode(x,y);
			}else{
				System.out.println("Falling position provided. This input will be ignored..!!");
			}
		}catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return tempGraphNode;
	}
}
